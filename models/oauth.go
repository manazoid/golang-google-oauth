package models

type InputValidate struct {
	Code   string `json:"code"`
	Secret string `json:"secret"`
}

type InputGenerate struct {
	Account string `json:"account"`
}

type OutputGenerate struct {
	Secret string `json:"secret"`
	QR     string `json:"qr"`
}
