package oauth

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"googleAuth/models"
	"io/ioutil"
	"net/http"
)

const (
	key    = "d2754aa7ddmsh1d3e9f6b58c1aebp1980bbjsn9d47a65bc468"
	issuer = "CompanyName"
)

// Generate returns output generate model
func Generate(c *gin.Context) {
	var output models.OutputGenerate
	var input models.InputGenerate

	if err := c.BindJSON(&input); err != nil {
		c.Status(http.StatusBadRequest)
		return
	}

	if input.Account == "" {
		c.JSON(http.StatusBadRequest, "Значение account не может быть равно нулю")
		return
	}

	url := "https://google-authenticator.p.rapidapi.com/new_v2/"

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("X-RapidAPI-Host", "google-authenticator.p.rapidapi.com")
	req.Header.Add("X-RapidAPI-Key", key)
	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	output.Secret = string(body)

	enrollResponse, err := Enroll(output.Secret, issuer, input.Account)
	if err != nil {
		c.Status(http.StatusInternalServerError)
	}
	output.QR = enrollResponse

	c.JSON(res.StatusCode, output)
}

//Enroll returns QR-Code image URL
func Enroll(secret string, issuer string, account string) (string, error) {
	url := fmt.Sprintf("https://google-authenticator.p.rapidapi.com/enroll/?secret=%s&issuer=%s&account=%s",
		secret, issuer, account)

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("X-RapidAPI-Host", "google-authenticator.p.rapidapi.com")
	req.Header.Add("X-RapidAPI-Key", key)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	result := string(body)
	if res.StatusCode != 200 {
		return "", errors.New(string(http.StatusBadRequest))
	}
	return result, nil
}

// Validate returns http status code to gin context
func Validate(c *gin.Context) {
	//code string, secret string
	var response models.InputValidate

	if err := c.BindJSON(&response); err != nil {
		fmt.Println(err)
		return
	}
	if response.Code == "" || response.Secret == "" {
		c.JSON(http.StatusBadRequest, "Значение Code или Secret не может быть равно нулю")
		return
	}

	url := fmt.Sprintf(`https://google-authenticator.p.rapidapi.com/validate/?code=%s&secret=%s`,
		response.Code, response.Secret)

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("X-RapidAPI-Host", "google-authenticator.p.rapidapi.com")
	req.Header.Add("X-RapidAPI-Key", key)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	result := string(body)

	if result != "True" {
		c.Status(http.StatusInternalServerError)
		return
	}

	c.Status(http.StatusOK)
}
