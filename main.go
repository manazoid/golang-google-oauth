package main

import (
	"github.com/gin-gonic/gin"
	"googleAuth/oauth"
)

func main() {
	// http request handler – GIN
	router := gin.Default()
	// localhost:8080/<here handler name>
	router.POST("/generate", oauth.Generate)
	router.POST("/validate", oauth.Validate)

	router.Run(":8080")
}
